/**
 * 
 */
package com.kampyacr.demo.java8.inicializacion;

/**
 * @author kampyacr
 *
 */
public class Bloque extends SuperBloque {
	{
		//Depeus del constructor super
		System.out.println("Bloque 1");
	}
	static {
		//Al cargar la clase
		System.out.println("Bloque static");
	}

	/**
	 * 
	 */
	public Bloque() {
		System.out.println("Constructor");
	}
	
	public static void main(String[] args) {
		System.out.println("Mainn");
		Bloque bloque = new Bloque();
		
		//Instancia el bloque estatico al llamar a un statico o al crar el objetoWs
		BloqueEstatico bloqueEstatico = new BloqueEstatico();
				
	}

}
