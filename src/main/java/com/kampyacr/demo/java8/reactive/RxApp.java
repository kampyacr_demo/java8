/**
 * 
 */
package com.kampyacr.demo.java8.reactive;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Action1;

/**
 * @author kampyacr
 *
 */
public class RxApp {
	private List<Integer> lista1;
	private List<Integer> lista2;
	
	public RxApp() {
		lista1 = new ArrayList<Integer>();
		lista2 = new ArrayList<Integer>();
		this.llenarListas();
		
	}
	
	public void llenarListas() {
		for(int i= 0; i<10;i++) {
			lista1.add(i);
			lista2.add(i);
		}
		
		
	}
	
	public void buscar() {
		Observable<Integer> obs1 = Observable.from(lista1);
		Observable<Integer> obs2 = Observable.from(lista2); 
		
//		Observable.merge(obs1, obs2).subscribe(new Action1<Integer>() {
//
//			@Override
//			public void call(Integer numero) {
//				if(numero == 1) {
//					System.out.println(numero);
//				}
//				
//			}
//		});
		
		Observable.merge(obs1, obs2).filter(x -> x==1).subscribe(System.out::println);
		Observable.merge(obs1, obs2).filter(x -> x==1).subscribe(x ->{
			//Mas logica
			if(x==1) {
				System.out.println(x);
			}
		});
		
	}
	
	public static void main(String[] args) {
		List<String> frutas = new ArrayList<>();
		frutas.add("Pera");
		frutas.add("Naranja");
		frutas.add("Manzana");
		frutas.add("Fresa");
		
		Observable<String> obs = Observable.from(frutas);
		obs.subscribe(new Action1<String>() {

			@Override
			public void call(String elemento) {
				System.out.println(elemento);
				
			}
		});
		
		RxApp app = new RxApp();
		app.buscar();
	}
	
	
 

}
