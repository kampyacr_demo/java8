package com.kampyacr.demo.java8.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class MapApp {
	
	private  Map<Integer, String> map;
	
	public void poblar() {
		map = new HashMap<>();
		map.put(1, "Pera");
		map.put(2, "Melocoton");
		map.put(3, "Manzana");
	}
	
	public void imprimir_v7() {
		for(Entry<Integer, String> e : map.entrySet()) {
			System.out.println("Clave: " + e.getKey() + " Valor: " + e.getValue());
		}
	}
	
	public void imprimir_v8() {
		map.forEach((k,v) -> System.out.println("Clave: " + k + " Valor: " + v));
		map.entrySet().stream().forEach(System.out::println);
	}
	
	public void insertarSiAusente() {
		map.putIfAbsent(4, "Naranja");
	}
	
	public void operarSiPresente() {
		map.computeIfPresent(2, (k,v) -> v.concat("---"));
		System.out.println(map.get(2));
	}
	
	public void obtenerOrPredeterminado() {
		String valor = map.getOrDefault(6, "Default");
		System.out.println(valor);
	}
	
	public void recolectar() {
		System.out.println("Recolecar");
		Map<Integer, String> mapaRecolectado = map.entrySet().stream()
				.filter(e->e.getValue().contains("o"))
				.collect(Collectors.toMap(p-> p.getKey(), p-> p.getValue()));
		mapaRecolectado.entrySet().stream().forEach(System.out::println);
	}

	public static void main(String[] args) {
		MapApp app = new MapApp();
		app.poblar();
		//app.imprimir_v7();
		app.imprimir_v8();
		app.insertarSiAusente();
		app.imprimir_v8();
		app.operarSiPresente();
		app.obtenerOrPredeterminado();
		app.recolectar();

	}

}
