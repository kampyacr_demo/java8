/**
 * 
 */
package com.kampyacr.demo.java8.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kampyacr
 *
 */
public class AppMain {
	
	private List<String> frutas;
	
	void llenarLista() {
		frutas = new ArrayList<>();
		frutas.add("Pera");
		frutas.add("Naranja");
		frutas.add("Fresa");
		frutas.add("Manzan");
		
	}
	
	void usarForEach() {
//		
//	for (String fruta : frutas) {
//			System.out.println(fruta);
//		}	
		//
//		frutas.forEach(x -> System.out.println(x));
		//
		frutas.forEach(System.out::println);
		
	}
	
	void usarRemoveIf() {
//		frutas.removeIf(x -> x.equalsIgnoreCase("Fresa"));
		//
		frutas.removeIf("Fresa":: equalsIgnoreCase);
	}
	
	void usarSort() {
//		frutas.sort((x,y) -> x.compareTo(y));
		///
		frutas.sort(String:: compareTo);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AppMain main = new AppMain();
		main.llenarLista();
		main.usarForEach();
		main.usarRemoveIf();
		main.usarSort();
		main.usarForEach();

	}

}
