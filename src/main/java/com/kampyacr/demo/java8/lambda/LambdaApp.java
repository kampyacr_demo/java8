/**
 * 
 */
package com.kampyacr.demo.java8.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author kampyacr
 *
 */
public class LambdaApp {
	
	private static  List<String> lista;
	
	public void inizializarLista() {
		lista = new ArrayList<>();
		lista.add("Perro");
		lista.add("Gato");
		lista.add("Conejo");
		lista.add("Pez");
	}

	public void ordenar() {
		lista.sort(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
	}
	
	public void ordenarLambda() {
		Collections.sort(lista, (String parametro1, String parametro2) -> parametro1.compareTo(parametro2) );
	}
	
	public void display() {
		for (String string : lista) {
			System.out.println(string);
		}
	}
	
	public void calcular() {
		Operacion operacion = (double x, double y) -> (x+y)/2;
		System.out.println(operacion.calcularPromedio(7, 8));
	}
	
	public static void main(String[] args) {
		LambdaApp lambdaApp = new LambdaApp();
	
		System.out.println("Sin lambda");
		System.out.println("-----------");
		lambdaApp.inizializarLista();
		lambdaApp.display();
		lambdaApp.ordenar();
		System.out.println("-----------");
		System.out.println("-----------");
		lambdaApp.display();
		
		System.out.println("Con lambda");
		System.out.println("-----------");
		lambdaApp.inizializarLista();
		lambdaApp.display();
		lambdaApp.ordenarLambda();
		System.out.println("-----------");
		System.out.println("-----------");
		lambdaApp.display();
		System.out.println("-----------");
		System.out.println("-----------");
		lambdaApp.calcular();
	}
}
