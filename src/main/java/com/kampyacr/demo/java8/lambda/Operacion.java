/**
 * 
 */
package com.kampyacr.demo.java8.lambda;

/**
 * @author kampyacr
 *
 */
public interface Operacion {
	double calcularPromedio(double n1, double n2);
}
