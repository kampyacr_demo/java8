/**
 * 
 */
package com.kampyacr.demo.java8.defaulmethods;

/**
 * @author kampyacr
 *
 */
public interface Pez {

	default void nadar() {
		System.out.println("Esta nadando");
	}
}
