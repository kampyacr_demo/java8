package com.kampyacr.demo.java8.stream;

import java.util.ArrayList;
import java.util.List;

public class StreamApp {

	private List<String> frutas;
	private List<String> numeros;

	public StreamApp() {
		frutas = new ArrayList<String>();
		frutas.add("Naranja");
		frutas.add("Manzana");
		frutas.add("Pera");
		frutas.add("Melocoton");

		numeros = new ArrayList<String>();
		numeros.add("1");
		numeros.add("2");
	}

	public void filtrar() {
		frutas.stream().filter(x -> x.startsWith("M")).forEach(System.out::println);
	}

	public void ordenar() {
		frutas.stream().sorted().forEach(System.out::println);
		frutas.stream().sorted((x,y)-> y.compareTo(x)).forEach(System.out::println);
	}

	public void transformar() {
		frutas.stream().map(String::toUpperCase).forEach(System.out::println);
		numeros.stream().map(x -> Integer.parseInt(x) + 1).forEach(System.out::println);
	}

	public void limitar() {
		frutas.stream().limit(2).forEach(System.out::println);
	}

	public void contar() {
		long total = frutas.stream().count();
		System.out.println(total);
	}

	public static void main(String[] args) {
		StreamApp app = new StreamApp();
		System.out.println("Filtrar");
		System.out.println("-----------------");
		app.filtrar();
		System.out.println("Ordenar");
		System.out.println("-----------------");
		app.ordenar();
		System.out.println("Transformar");
		System.out.println("-----------------");
		app.transformar();
		System.out.println("Limitar");
		System.out.println("-----------------");
		app.limitar();
		System.out.println("Contar");
		System.out.println("-----------------");
		app.contar();
	}
}
