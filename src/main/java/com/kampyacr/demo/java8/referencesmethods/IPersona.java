package com.kampyacr.demo.java8.referencesmethods;

@FunctionalInterface
public interface IPersona {

	Persona crear(int id, String nombre);
}
