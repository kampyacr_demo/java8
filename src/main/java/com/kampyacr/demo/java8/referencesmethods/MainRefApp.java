/**
 * 
 */
package com.kampyacr.demo.java8.referencesmethods;

import java.util.Arrays;
import java.util.Comparator;

/**O
 * @author kampyacr
 *
 */
public class MainRefApp {

	public static void referenciaMetodoStatic() {
		System.out.println("referenciaMetodoStatic");
	}
	
	public void referenciaMetodoinstanciaObjetoArbitrario() {
		String[] frutas = {"Pera", "Manzara", "Naranja"};
		
		Arrays.sort(frutas, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);
			}
		} );
		
		System.out.println(frutas);
		//
		Arrays.sort(frutas, (x1, x2) -> x1.compareToIgnoreCase(x2));
		System.out.println(frutas);
		//
		Arrays.sort(frutas, String :: compareToIgnoreCase);
		System.out.println(frutas);
		
	}
	
	public void referenciaMetodoInstanciaObjetoParticular() {
		System.out.println("Metodo rerefercia objeto particular");
	}
	
	public void referenciaConstructor() {
		IPersona iper = new IPersona() {
			
			@Override
			public Persona crear(int id, String nombre) {
				return new Persona(id, nombre);
			}
		};
		
		Persona p1 = iper.crear(1, "Fernando");
		System.out.println(p1);
		
		///
		IPersona iPerLam = (id, nombre) -> ( new Persona(id,nombre));
		Persona p2 =iPerLam.crear(2, "Maria");
		System.out.println(p2);
		//
		IPersona iPerRef = Persona :: new;
		Persona p3 =iPerRef.crear(3, "José");
		System.out.println(p3);
		
	}
	
	public void operar() {
		//
		Operacion op = () -> MainRefApp.referenciaMetodoStatic();
		op.saludar();
		
		Operacion op2 = MainRefApp :: referenciaMetodoStatic;
		op2.saludar();
		//
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MainRefApp app = new MainRefApp();
		app.operar();
		app.referenciaMetodoinstanciaObjetoArbitrario();
		Operacion op = app :: referenciaMetodoInstanciaObjetoParticular;
		op.saludar();
		app.referenciaConstructor();
	}

}
