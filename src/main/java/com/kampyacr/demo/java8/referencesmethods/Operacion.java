package com.kampyacr.demo.java8.referencesmethods;

@FunctionalInterface
public interface Operacion {

	void saludar();
}
