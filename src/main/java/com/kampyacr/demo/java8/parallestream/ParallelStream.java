package com.kampyacr.demo.java8.parallestream;

import java.util.ArrayList;
import java.util.List;

//No recomendable para EE.
//Sipara batcch etc
public class ParallelStream {
	private List<Integer> numeros;
	
	public void llenar() {
		numeros = new  ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			numeros.add(i);
		}
	}
	
	public void stream() {
		numeros.stream().forEach(System.out::println);
	}
	
	public void streamParalelo() {
		numeros.parallelStream().forEach(System.out::println);
	}
	
	
	public static void main(String[] args) {
		ParallelStream app = new ParallelStream();
		app.llenar();
		//app.stream();
		app.streamParalelo();
	}

}
