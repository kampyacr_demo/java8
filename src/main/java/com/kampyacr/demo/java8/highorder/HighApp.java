package com.kampyacr.demo.java8.highorder;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class HighApp {

	private Function<String, String> convertirMayusculas = x -> x.toUpperCase();
	private Function<String, String> convertirMinusculas = x -> x.toLowerCase();
	
	
	public void imprimir(Function<String, String> funcion, String valor) {
		System.out.println(funcion.apply(valor));
	}
	
	public Function<String, String> mostrar(String mensaje){
		return (String x) -> mensaje + x;
	}
	
	public void filtrar(List<String> lista, Consumer<String> consumidor, int longitud) {
		lista.stream().filter(establecerLogicaFiltro(longitud)).forEach(consumidor);
	}
	
	public void filtrar(List<String> lista, Consumer<String> consumidor, String cadena) {
		lista.stream().filter(establecerLogicaFiltro(cadena)).forEach(consumidor);
	}
	
	public Predicate<String> establecerLogicaFiltro(int longitud) {
		return texto -> texto.length() < longitud;
	}
	
	public Predicate<String> establecerLogicaFiltro(String cadena) {
		return texto -> texto.contains(cadena);
	}
	
	public static void main(String[] args) {
		HighApp app = new HighApp();
		app.imprimir(app.convertirMayusculas, "minusculas");
		app.imprimir(app.convertirMinusculas, "MAYUSCULAS");
		
		String rpta = app.mostrar("Hola ").apply("amigo");
		System.out.println(rpta);
		
		List<String>frutas = new ArrayList<>();
		frutas.add("Pera");
		frutas.add("Naranja");
		frutas.add("Fresa");
		frutas.add("Manzana");
		
		app.filtrar(frutas, System.out::println, 5);
		app.filtrar(frutas, System.out::println, "sa");
		
	}

}
