package com.kampyacr.demo.java8.date;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DateApp {

	
	public void verificar() {
		LocalDate fecha1 = LocalDate.of(1996, 3, 16);
		LocalDate fecha2 = LocalDate.now();
		
		System.out.println(fecha1.isAfter(fecha2));
		System.out.println(fecha1.isBefore(fecha2));
		
		LocalTime tiempo1 = LocalTime.of(22,15, 16);
		LocalTime tiempo2 = LocalTime.now();
		
		System.out.println(tiempo1.isAfter(tiempo2));
		System.out.println(tiempo1.isBefore(tiempo2));
		
		LocalDateTime fechaTiempo1 = LocalDateTime.of(1996, 3, 16, 22,58,47);
		LocalDateTime fechaTiempo2 = LocalDateTime.now();
		
		System.out.println(fechaTiempo1.isAfter(fechaTiempo2));
		System.out.println(fechaTiempo1.isBefore(fechaTiempo2));
		
	}
	
	public void meditTiempo() throws InterruptedException {
		Instant ini = Instant.now();
		Thread.sleep(1000);
		Instant fin = Instant.now();
		
		System.out.println("Tiempo " + Duration.between(ini, fin).toMillis());
	}
	
	
	public void peridoEntreFechas() {
		LocalDate nacimiento = LocalDate.of(1996, 10, 14);
		LocalDate actual = LocalDate.now();
		
		Period periodo = Period.between(nacimiento, actual);
		
		System.out.println("Han pasado " + periodo.getYears() + " años," +
		 periodo.getMonths() + " meses y " + periodo.getDays() + " dias. Desde " +
				nacimiento + " hasta " + actual);
	}
	
	public void convertir() {
		String fecha = "10/12/1996";
		DateTimeFormatter fomater = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaLocal = LocalDate.parse(fecha, fomater);
		System.out.println(fechaLocal);
		
		fomater = DateTimeFormatter.ofPattern("ddMMyyyy");
		System.out.println(fechaLocal.format(fomater));
				
	}
	
	public static void main(String[] args) throws Exception {
		DateApp app = new DateApp();
		app.verificar();
		app.meditTiempo();
		app.peridoEntreFechas();
		app.convertir();
	}
}
