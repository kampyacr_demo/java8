/**
 * 
 */
package com.kampyacr.demo.java8.optional;

import java.util.Optional;

/**
 * @author kampyacr
 *
 */
public class OptionalApp {
	
	public void probar(String valor) {
		System.out.println(valor.contains("nombre"));
	}
	
	
	public void orElse(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		String x = op.orElse("Predeteminado");
		System.out.println(x);
	}
	
	public void orElseThow(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		op.orElseThrow(NumberFormatException:: new
				);
	}
	
	public void isPresent(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		System.out.println(op.isPresent());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		OptionalApp app = new OptionalApp();
		app.orElse(null);
		app.orElse("pruebas");
		
		app.isPresent(null);
		
		app.orElseThow(null);

	}

}
